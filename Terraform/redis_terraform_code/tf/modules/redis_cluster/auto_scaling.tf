resource "aws_launch_configuration" "my_conf" {
  name_prefix   = "my_lc"
  image_id      = "ami-035b3c7efe6d061d5"
  instance_type = "t2.micro"
  key_name      = "${var.my_key_pair}"
  security_groups = ["${aws_security_group.redis_sec_group.id}"]

lifecycle {
  create_before_destroy =  "true"
 }
}

resource "aws_autoscaling_group" "my_asg1" {
  name                      = "autogroup1"
  max_size                  = 3
  min_size                  = 3
  health_check_grace_period = 300
  health_check_type         = "EC2"
  desired_capacity          = 3
  force_delete              = true
  launch_configuration      = "${aws_launch_configuration.my_conf.name}"
  vpc_zone_identifier       = ["${aws_subnet.redis_subnet_priv1.id}"]

#tags = concat(
#    [
#      {
#        "key"                 = "Name"
#        "value"               = var.tags_master
#        "propagate_at_launch" = true
#      },
#    ],
#    var.tags,
###    local.tags_asg_format,
#  )
#
lifecycle {
  create_before_destroy = true
 }
}


resource "aws_autoscaling_group" "my_asg2" {
 name                      = "autogroup2"
 max_size                  = 3
 min_size                  = 3
 health_check_grace_period = 300
 health_check_type         = "EC2"
 desired_capacity          = 3
 force_delete              = true
 launch_configuration      = "${aws_launch_configuration.my_conf.name}"
 vpc_zone_identifier       = ["${aws_subnet.redis_subnet_priv2.id}"]

#tags = [
#{
#  key = "Name"
#  value = "slave1"
#  propagate_at_launch = true
#},
#{
#  key = "Name"
#  value = "slave2"
#  propagate_at_launch = true
#},
#{
#  key = "Name"
#  value = "slave3"
#  propagate_at_launch = true
#},
#] 

#tags = ["${concat(
#   list(
#map("key", "Name", "value", "slave1", "propagate_at_launch", true),
#map("key", "Name", "value", "slave2", "propagate_at_launch", true),
#map("key", "Name", "value", "slave3", "propagate_at_launch", true)
#    ),
#    )
#}"]

lifecycle {
  create_before_destroy = true
 }
}
