resource "aws_network_acl" "my_nacl" {
  vpc_id = "${aws_vpc.redis_vpc.id}"
 }

resource "aws_network_interface" "test" {
  subnet_id       = "${aws_subnet.redis_subnet_priv1.id}"
#  private_ips    = ["10.0.0.50"]
  #  availability_zone = "{var.my_availability_zone1}"
  security_groups = ["${aws_security_group.redis_sec_group.id}"]
} 
