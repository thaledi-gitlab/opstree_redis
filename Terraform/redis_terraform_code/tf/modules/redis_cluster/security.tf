#resource "aws_security_group" "my_security_group" {
#  description = "Rules to allow incoming requests"
#  vpc_id = "${aws_vpc.my_vpc_cidr.id}"
#}

resource "aws_security_group" "redis_sec_group" {
  description = "Allow TLS inbound traffic"
  vpc_id = "${aws_vpc.redis_vpc.id}"

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {

    from_port   = 23
    to_port     = 23
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
   # prefix_list_ids = ["pl-12c4e678"]
  }
 
 tags = {
   Name = "redis_security_group"
 }
}
