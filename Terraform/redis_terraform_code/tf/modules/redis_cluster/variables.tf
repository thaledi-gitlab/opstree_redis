variable "my_vpc_cidr" {
  description = "CIDR block for the VPC"
  default = "10.0.0.0/16"
}

variable "my_public_subnet1_cidr" {
  description = "CIDR block for public subnet1"
  default = "10.0.0.0/17"
}

variable "my_private_subnet1_cidr" {
  description = "CIDR block for private subnet1"
  default = "10.0.128.0/18"
}

 variable "my_private_subnet2_cidr" {
   description = "CIDR block for private subnet2"
   default = "10.0.192.0/19"
}

variable "my_private_subnet3_cidr" {
   description = "CIDR block for private subnet3"
   default = "10.0.224.0/20"
}

variable "my_private_subnet4_cidr" {
   description = "CIDR block for private subnet4"
   default = "10.0.240.0/20"
}

 variable "my_availability_zone1" {
   description = "Availability zone for Public Subnet1, Private Subnet1 & Private Subnet2"
   default = "us-east-1a"
}

variable "my_availability_zone2" {
  description = "Availability zone for Private Subnet3 & Private Subnet4"
  default = "us-east-1b"
}

variable "my_ec2_instance_ami" {
  description = "EC2 instances amis for the redis cluster"
  default = "ami-035b3c7efe6d061d5"
}

variable "my_ec2_instance_type" {
  description = "EC2 instances type for the ami-0080e4c5bc078760e"
  default = "t2.micro"
}

variable "number" {
  description = "Number of instances launched inside the subnets"
  default = "3"
}

variable "my_key_pair" {
  description = "Key pair for logging into my EC2 instances"
  default = "dev_ops_auto"
}
